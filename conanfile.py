#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, CMake, tools
import os

class HelloConan(ConanFile):
    name = "hello"
    version = "0.1.0"
    description = "Simple library to test conan"
    # topics that can get used for searches:
    topics = ("conan", "cmake", "cpp")
    homepage = "https://github.com/memsharded/hello"
    url = "https://github.com/Hopobcn/hello"
    author = "Hopobcn"
    license = "MIT"
    exports = ["LICENSE"]
    exports_sources = [ "CMakeLists.txt", "include/*", "src/*", "test/*"]

    generators = "cmake"

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared":False, "fPIC":True}

    # Custom folders
    _source_subfolder = "./"
    _build_subfolder = "hello"

    # requirements
    requires = {}

    def config_options(self):
        if self.settings.os == 'Windows':
            del self.options.fPIC

    #def source(self):
    #    source_url = "https://github.com/Hopobcn/hello"
    #    self.run("git clone " + source_url + "-b master")
    #    self.run("cd hello")

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["ENABLE_TESTS"] = False
        cmake.configure(build_folder=self._build_subfolder)
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        self.copy(pattern="LICENSE", dst="licenses", src=self._source_subfolder)
        cmake = self._configure_cmake()
        cmake.install()
        include_foder = os.path.join(self._source_subfolder, "include")
        self.copy(pattern="*", dst="include", src="include")
        self.copy(pattern="*.dll", dst="bin", keep_path=False)
        self.copy(pattern="*.lib", dst="lib", keep_path=False)
        self.copy(pattern="*.a", dst="lib", keep_path=False)
        self.copy(pattern="*.so", dst="lib", keep_path=False)
        self.copy(pattern="*.dylib", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

