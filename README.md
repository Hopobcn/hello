# Conan package example


## Howto:

1. Install conan
2. Checkout this repo
3. Create a build directory
4. Create the conan package:
```
conan create .. hello/testing --build missing
```